from pyexpat import model
from typing import AbstractSet
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    name=models.CharField(max_length=150,verbose_name='Nombre')
    code=models.CharField(max_length=150,verbose_name='Codigo', blank=True)
    region=models.CharField(max_length=30,verbose_name='Region', blank=True)
    office=models.CharField(max_length=60,verbose_name='Oficina', blank=True)
    job=models.CharField(max_length=150,verbose_name='Puesto',blank=True)
    verified=models.IntegerField(default=0)

    #def __str__(self):
#        return self

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.set_password(self.password)
        super().save(*args, **kwargs)

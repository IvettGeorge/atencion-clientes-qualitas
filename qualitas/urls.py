"""qualitas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core.views import *
from login.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index4,name="index"),
    path('home',home,name="home"),
    path('inicio',loginView,name="inicio"),
    #path('inicio2',loginView,name="inicio2"),
    
    path('register',register,name="register"),

    path('login',validar,name='login'),
    path('logout',LogoutRedirectView.as_view(), name="logout"),
    path('createQuestion/',QuestionCreateView.as_view(), name="createQuestion"),
    path('createSurvey/',SurveyCreateView.as_view(), name="createSurvey"),

    path('getQuiz',getQuiz ,name="getQuiz"),
    path('createQuizz/',getAnswers, name="createQuizz"),
    path('getSurvey',getSurveyStatus, name="getSurvey"),
    path('getInfoValidate',getInfoValidate,name="getInfoValidate"),
    path('saveUserInfo',saveUserInfo,name="saveUserInfo"),
    path('confirmacion',confirmacion,name="confirmacion"),
    path('questions',questions,name="questions"),    
    path('getQuestions',getQuestions,name="getQuestions"),    
    path('registerJS',registerJS,name="registerJS"),    
    path('getRegister',getRegister,name="getRegister"),    
    path('adminQuizz',adminQuizz,name="adminQuizz"),  
    path('getNotRegister',getNotRegister,name="getNotRegister"),    
    path('2',index2,name="index"),
    path('inicio2',loginView2,name="inicio2"),
    path('login2',validar2,name='login'),
    path('confirmacion2',confirmacion2,name="confirmacion"),
    path('3',index3,name="index3"),
    path('4',index4,name="inde4"),

]


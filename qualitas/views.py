from email import message
from email.message import EmailMessage
import encodings
import imp
from pickle import NONE
from re import template
import re
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, request, response
from django.http import HttpResponse, JsonResponse
import json
from user.forms import UserForm
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, request
from django.shortcuts import get_object_or_404, redirect, render
from login.forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView
from core.models import *
from core.forms import *
from user.models import *
from django.template.loader import render_to_string
from django.conf import settings 
from django.contrib import messages
from email.utils import formataddr

import smtplib
import ssl
import getpass

from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from django.core.serializers import serialize

from datetime import datetime
import boto3
import xlwt

def index(request):
    dictionary={
        'Title': 'Quañitas ODQ'
    }
    return render(request,'index2.html',dictionary)

def index2(request):
    dictionary={
        'Title': 'Quañitas ODQ'
    }
    return render(request,'indexSP1.html',dictionary)

def index3(request):
    dictionary={
        'Title': 'Quañitas ODQ'
    }
    return render(request,'indexSP2.html',dictionary)

def index4(request):
    dictionary={
        'Title': 'Quañitas ODQ'
    }
    return render(request,'index5.html',dictionary)

def home(request):
    if not request.user.is_authenticated:
        print("Login")
        print(request.user)
        return redirect('/inicio')    
    dictionary={
        'Title': 'Qualitas ODQ'
    }
    return render(request,'stream.html',dictionary)

@csrf_exempt
def register(request):
    data={}
    userData = json.loads(request.body)
    try:
        action=userData['action']
        if action == 'add':
            form=UserForm(userData)
            if form.is_valid():  
                print("a")                  
                form=form.save(True)
                data['mensaje']='Usuario Creado Correctamente'
                data['id_user_event_system']=form.id                    
                data['message']="200"
            else:
                        data['error']=form.errors
        else:
            data['error']='Error al registrar usuario'
    except Exception as e:
        data['error']=str(e)
        print(data)
    return JsonResponse(data)    

def inicio(request):
    dictionary={
        'Title': 'Quañitas ODQ'
    }
    return render(request,'inicio.html',dictionary)

def login(request):
    print(request)
    if request.user.is_authenticated:
            print(request.user)
            return redirect('/home')
    form = LoginForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            user = form.login(request)
            if user:
                login(request, user)
                return HttpResponseRedirect("/home")
        else:
            print("form no valid")
    else:
        form=LoginForm()
    return render(request, 'inicio.html', {'form':form})

def validar(request):
    dictionary={
        'Title': 'Qualitas ODQ'
    }
    return render(request,'login2.html',dictionary)    
    
class QuestionCreateView(CreateView):
    model=Question
    form_class=QuestionForm
    template_name='stream.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("Guardada")
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Question'
        context['action'] = 'add'
        return context

class SurveyCreateView(CreateView):
    model=Survey
    form_class=SurveyForm
    template_name='stream.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name)
    
    def post(self, request, *args, **kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'add':
                form=self.get_form()
                if form.is_valid():
                    form.save(True)
                    print("Guardada")
                    data['estado']="Encuesta guardada correctamente"
                else:
                    data['error']=form.errors
            else:
                data['error']='Error al enviar la pregunta'
        except Exception as e:
            data['error']=str(e)
            print(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):        
        context= super().get_context_data(**kwargs)
        context['title']='Crear Survey'
        context['action'] = 'add'
        return context

def getQuiz(request):
    quizz=Active.objects.filter(name='quizz')
    for quiz in quizz:
        quizzNumber=quiz.numberActive
    return JsonResponse({'data':quizzNumber})

@csrf_exempt
def getAnswers(request):
    data={}
    try:
        action=request.POST['action']
        if action == 'add':
            quizz=Quizz(
                quizz=request.POST['question'],
                answer=request.POST['answer'],
                user=request.POST['user'],
                email=request.POST['email'],
                office=request.POST['office'],
                region=request.POST['region']
            )
            quizz.save()
            answers=getAnswers2(request.POST['question'])
            data['answers']=answers['data']
            data['total']=answers['total']
        else:
            data['error']='Error al enviar la pregunta'    
    except Exception as e:
        data['error']=str(e)
        print(e)
        
    print(data)    
    return JsonResponse(data)

def getAnswers2(idQuizz):
    data={}
    trivias=Quizz.objects.filter(quizz=idQuizz)
    answers=[]
    for trivia in trivias:
        answers.append(trivia.answer)
    a=answers.count('A')
    b=answers.count('B')
    c=answers.count('C')
    d=answers.count('D')
    total=a+b+c+d
    data={'A':a,'B':b,'C':c,'D':d}
    context={'data':data,'total':total}
    return context

def getSurveyStatus(request):
    surveys=Active.objects.filter(name='survey')
    for survey in surveys:
        surveyNumber=survey.numberActive
    return JsonResponse({'data':surveyNumber})

@csrf_exempt
def getInfoValidate(request):
    data={}
    mail=request.POST['email']
    try:
        user=User.objects.get(email=mail)
        name=user.name
        office=user.office
        job=user.job
        id=user.id
        data['user']={'id':id,'name':name,'office':office,'job':job}
    except:
        print("no existe")
        data['error']='Error usuario no encontrado'
    return JsonResponse(data)

@csrf_exempt
def saveUserInfo(request):
    data={}
    user=request.POST
    try:
        user=User.objects.get(id=request.POST['id'])
        name=request.POST['name']
        email=request.POST['email']
        user.name=name
        user.office=request.POST['office']
        user.job=request.POST['job']
        user.verified=1
        user.save()
        sendMessages(name,email)
        data['message']="Usuario válidado correctamente"
    except:
        data['error']='Error usuario no encontrado'    
    return JsonResponse(data)    

def sendMessages(nombre,email):
    qemail=settings.EMAIL_HOST_USER
    qpass=settings.EMAIL_HOST_PASSWORD
    s = smtplib.SMTP('smtp.mailgun.org', 587)
    s.login(qemail,qpass)
    print("Inicio de sesion exitoso")
    destinatario = email
    text= createMensaje(email, qemail,nombre)
    s.sendmail(qemail, destinatario, text)
    print("Mensaje enviado")
    s.quit()

def createMensaje(usuario, username,nombre):
    mensaje=MIMEMultipart("alternative") #Mensaje estandar
    mensaje['Subject']="Confirmación Seminario Atención a Clientes"
    mensaje['From']=formataddr(("Seminario Atención a Clientes", "no-reply@atencionaclientesqualitas.mx"))
    mensaje['To']=usuario
    htmlTemplate=render_to_string('email.html', {'name':nombre})
    html_mail= MIMEText(htmlTemplate,"html")
    mensaje.attach(html_mail)

    text=mensaje.as_string()
    return text

def confirmacion(request):
    dictionary={
        'Title': 'Qualitas ODQ'
    }
    return render(request,'confirmacion2.html',dictionary)

def questions(request):     
    questions=Question.objects.all()
    dictionary={'questions':questions}
    return render(request,'questions.html',dictionary)

@csrf_exempt
def getQuestions(request):
    data={}
    try:      
        questions=Question.objects.all()
        questionsData=json.loads(serialize('json',questions))
        data={'questions':questionsData}
    except:
        data['error']="Error al obtener preguntas"
    return JsonResponse(data)

@csrf_exempt
def registerJS(request):
    return render(request,'register.html')

def getRegister(request):
    response=HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition']='attachment; filename=Registrados'+ str(datetime.now())+'.xls'
    wb=xlwt.Workbook(encoding='utf-8')

    ws=wb.add_sheet('Representantes')

    row_num=0
    font_style=xlwt.XFStyle()
    font_style.font.bold=True

    columns=['No.','Oficina','Representante','Correo','Zona','Estado']
    for col_num in range(len(columns)):
        ws.write(row_num,col_num,columns[col_num],font_style)    

    font_style=xlwt.XFStyle()
    rows=User.objects.filter(verified=1,job__contains="Representante").values_list('code','office','name','email','region')
    
    try:
        for row in rows:
            row_num+=1
            for col_num in range(len(row)):
                ws.write(row_num, col_num,str(row[col_num]),font_style)
            ws.write(row_num,len(row),'Registrado',font_style)

    except:
        print("no hay rows")

    wCoord=wb.add_sheet('Coordinadores')
    rowCoord=0
    font_styleCoord=xlwt.XFStyle()
    font_styleCoord.font.bold=True

    colCoord=['Coordinador','Correo','Zona','Estado']
    for ncolCoord in range(len(colCoord)):
        wCoord.write(rowCoord,ncolCoord,colCoord[ncolCoord],font_styleCoord)    

    font_styleCoord=xlwt.XFStyle()
    coordinadores=User.objects.filter(verified=1,job__contains="Coordinador").values_list('name','email','region')
    
    try:
        for coordinador in coordinadores:
            rowCoord+=1
            for ncolCoord in range(len(coordinador)):
                wCoord.write(rowCoord, ncolCoord,str(coordinador[ncolCoord]),font_styleCoord)
            wCoord.write(rowCoord,len(coordinador),'Registrado',font_style)

    except:
        print("no hay coord")

    wDir=wb.add_sheet('Directores')
    rowDir=0
    font_styleDir=xlwt.XFStyle()
    font_styleDir.font.bold=True

    colDir=['Director','Correo','Zona','Estado']
    for ncolDir in range(len(colDir)):
        wDir.write(rowDir,ncolDir,colDir[ncolDir],font_styleDir)    

    font_styleDir=xlwt.XFStyle()
    directores=User.objects.filter(verified=1,job__contains="Director").values_list('name','email','region')
    
    try:
        for director in directores:
            rowDir+=1
            for ncolDir in range(len(director)):
                wDir.write(rowDir, ncolDir,str(director[ncolDir]),font_styleDir)
            wDir.write(rowDir,len(director),'Registrado',font_styleDir)
    except:
        print("no hay dire")

    wCor=wb.add_sheet('Corporativo')
    rowCor=0
    font_styleCor=xlwt.XFStyle()
    font_styleCor.font.bold=True

    colCor=['Corporativo','Correo','Zona','Oficina','Estado']
    for ncolCor in range(len(colCor)):
        wCor.write(rowCor,ncolCor,colCor[ncolCor],font_styleCor)    

    font_styleCor=xlwt.XFStyle()
    corporativos=User.objects.filter(verified=1,job__contains="Corporativo").values_list('name','email','region','code')
    
    try:
        for corporativo in corporativos:
            rowCor+=1
            for ncolCor in range(len(corporativo)):
                wCor.write(rowCor, ncolCor,str(corporativo[ncolCor]),font_styleCor)
            wCor.write(rowCor,len(corporativo),'Registrado',font_styleCor)
    except:
        print("no hay corporativo")

    wb.save(response)

    return response

def getNotRegister(request):
    response=HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition']='attachment; filename=Pendientes'+ str(datetime.now())+'.xls'
    wb=xlwt.Workbook(encoding='utf-8')

    ws=wb.add_sheet('Directivos')

    row_num=0
    font_style=xlwt.XFStyle()
    font_style.font.bold=True

    columns=['No.','Oficina','Representante','Correo','Zona','Estado']
    for col_num in range(len(columns)):
        ws.write(row_num,col_num,columns[col_num],font_style)    

    font_style=xlwt.XFStyle()
    rows=User.objects.filter(verified=0,job__contains="Representante").values_list('code','office','name','email','region')
    
    try:
        for row in rows:
            row_num+=1
            for col_num in range(len(row)):
                ws.write(row_num, col_num,str(row[col_num]),font_style)
            ws.write(row_num,len(row),'No Registrado',font_style)

    except:
        print("no hay rows")

    wCoord=wb.add_sheet('Coordinadores')
    rowCoord=0
    font_styleCoord=xlwt.XFStyle()
    font_styleCoord.font.bold=True

    colCoord=['Coordinador','Correo','Zona','Estado']
    for ncolCoord in range(len(colCoord)):
        wCoord.write(rowCoord,ncolCoord,colCoord[ncolCoord],font_styleCoord)    

    font_styleCoord=xlwt.XFStyle()
    coordinadores=User.objects.filter(verified=0,job__contains="Coordinador").values_list('name','email','region')
    
    try:
        for coordinador in coordinadores:
            rowCoord+=1
            for ncolCoord in range(len(coordinador)):
                wCoord.write(rowCoord, ncolCoord,str(coordinador[ncolCoord]),font_styleCoord)
            wCoord.write(rowCoord,len(coordinador),'No Registrado',font_style)

    except:
        print("no hay coord")

    wDir=wb.add_sheet('Directores')
    rowDir=0
    font_styleDir=xlwt.XFStyle()
    font_styleDir.font.bold=True

    colDir=['Director','Correo','Zona','Estado']
    for ncolDir in range(len(colDir)):
        wDir.write(rowDir,ncolDir,colDir[ncolDir],font_styleDir)    

    font_styleDir=xlwt.XFStyle()
    directores=User.objects.filter(verified=0,job__contains="Director").values_list('name','email','region')
    
    try:
        for director in directores:
            rowDir+=1
            for ncolDir in range(len(director)):
                wDir.write(rowDir, ncolDir,str(director[ncolDir]),font_styleDir)
            wDir.write(rowDir,len(director),'No Registrado',font_styleDir)
    except:
        print("no hay dire")

    wCor=wb.add_sheet('Corporativo')
    rowCor=0
    font_styleCor=xlwt.XFStyle()
    font_styleCor.font.bold=True

    colCor=['Corporativo','Correo','Zona','Oficina','Estado']
    for ncolCor in range(len(colCor)):
        wCor.write(rowCor,ncolCor,colCor[ncolCor],font_styleCor)    

    font_styleCor=xlwt.XFStyle()
    corporativos=User.objects.filter(verified=0,job__contains="Corporativo").values_list('name','email','region','code')
    
    try:
        for corporativo in corporativos:
            rowCor+=1
            for ncolCor in range(len(corporativo)):
                wCor.write(rowCor, ncolCor,str(corporativo[ncolCor]),font_styleCor)
            wCor.write(rowCor,len(corporativo),'No Registrado',font_styleCor)
    except:
        print("no hay corporativo")

    wb.save(response)

    return response
    
def adminQuizz(request):
      
    dictionary={
        'Title': ''
    }
    return render(request,'adminQuizz.html',dictionary)

def validar2(request):
    dictionary={
        'Title': 'Qualitas ODQ'
    }
    return render(request,'login2.html',dictionary)    
    

def confirmacion2(request):
    dictionary={
        'Title': 'Qualitas ODQ'
    }
    return render(request,'confirmacion2.html',dictionary)


function initConectados(){
					
	var confiig =KEY_QUALITAS_ATC_CONFIG;
		
	var parameters={};
	doConectados(parameters, confiig  );
			
}
//-----


//Version de accesos que únicamente muestra el número de usuarios conectados
var connectionConectados;
var usersconectados= [];
var connectionsconectados= [];

var configConectados_;
var parametersConectados_;
var renderConectadosCallback_;

function doConectados(  parameters, configchat) {
	parametersConectados_=parameters;
	configConectados_= configchat;
	renderConectadosCallback_= renderJustTotalConectados;// esta linea indica la funcion para renderizar

	connectionConectados = new ReconnectingWebSocket(configConectados_.access_service);	
			
	connectionConectados.onopen = (event) => {	
		console.log(" conectados is open now.");					
				
    };

    connectionConectados.onclose = (event) => {
      console.log(" conectados is closed now.");
	  
    };

    connectionConectados.onerror = (event) => {
      console.error(" error observed:", event);
    };

    connectionConectados.onmessage = (event) => {
	 			
		const data = JSON.parse(event.data);    
        
		const user ={			
			timestamp	: Date.now(),           
			username	: data.username,			
			nombre		: data.nombre,
            email		: data.email,	
			office		: data.office,
			job			: data.job,					
			region		: data.region,				
			idDetail	: data.idDetail,
			activo		: data.activo
		}		
		renderConectadosCallback_(user);
    };
}


renderJustTotalConectados = (user) => {
	if(user){		
		let display=displayuserconectado(user);		
		connectionsconectados.push({idDetail:user.idDetail});		
		usersconectados.push(user);

		if(display){
			$("#totalContactos").html("");
			$("#totalContactos").append(messageTotalConectados());			
		}
		return;
	}
	//usersconectados=obtainConectadosConnected();
	usersconectados=filteredconectadosbymail();
	
	$("#totalContactos").html("");
	$("#totalContactos").append(messageTotalConectados());	
}



messageTotalConectados = () => {
	return '<p><strong>'+usersconectados.length+' contactos</strong></p>';
}

function displayuserconectado(user){
	//var connmap = getMapConnectionsConectados();	
	var useremailmap=getmapconectadosbyemail();

	//let existeCon=user.activo && connmap.get(user.idDetail);
	let existeEmail=user.activo && useremailmap.get(user.email);
	let esSameUsuario=(user.email == parametersConectados_.email);//username 
	//if(existeCon ){
		//return false;
	//}
	if(existeEmail ){
		return false;
	}
	if(esSameUsuario){
		return false;
	}
	return true;
}

function obtainConectadosConnected(){
	var usermap = getmapconectadosbyconnections();
	var connmap = getMapConnectionsConectados();
	var usersfilteredDetail=[];
	for (var [clave, user] of usermap) {
		
		let existe=user.activo && connmap.get(user.idDetail);
		if(existe){
			usersfilteredDetail.push(user);
		}		
	}
	return usersfilteredDetail;
}


function filteredconectadosbymail(){
	var usermap =  new Map();
	var usersfiltered=[];
	for(var i = 0; i < usersconectados.length; i++ ){
		let user= usersconectados[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){	
			usermap.set(user.email,user);			
			usersfiltered.push(user);
		}
	}
	return usersfiltered;
}


function getmapconectadosbyconnections(){
	var usermap = new Map();
	
	for(var i = 0; i < usersconectados.length; i++ ){
		let user= usersconectados[i];
		
		let existe=usermap.get(user.idDetail); 
		if(!existe){
			usermap.set(user.idDetail,user);			
		}
	}
	return usermap;
}

function getmapconectadosbyemail(){
	var usermap = new Map();
	
	for(var i = 0; i < usersconectados.length; i++ ){
		let user= usersconectados[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){
			usermap.set(user.email,user);			
		}
	}
	return usermap;
}


function getMapConnectionsConectados(){
	var connmap = new Map();
	
	for(var i = 0; i < connectionsconectados.length; i++ ){
		let con= connectionsconectados[i];
		
		let existe=connmap.get(con.idDetail); 
		if(!existe){
			connmap.set(con.idDetail,con);			
		}
	}
	return connmap;
}

function lastUsersConectados(){
	
	var usersTmp= [];
	var connectionsTmp=[];						

	axios.post(configConectados_.table_access_service+"/details", { 	               
	}).then(function (response) {
			//console.log(response.data);						
			var items = JSON.parse(response.data.items);//body.items
			for (var i =0; i<items.length ;i++) {
				let item = items[i];								
				usersTmp.push({
					timestamp	: item.createdat,
					username	: item.username,										
					nombre		: item.nombre,
					office      :   item.office,
	                job         :   item.job,
	                region      :   item.region,
	                code        :   item.code,	
					email		:   item.email,
					idDetail	:	item.idDetail,
					activo		:   item.activo					
				});				
			}
			var conns = JSON.parse(response.data.connections);//body.connections
			for (var i =0; i<conns.length ;i++) {
				let con = conns[i];
				connectionsTmp.push({idDetail:con.connectionId});
			}
			
			usersconectados = usersTmp;
			connectionsconectados = connectionsTmp;

			renderConectadosCallback_();		
    }).catch(function (error) {
		console.log(error);
    });
}

function refreshDatatable (){
	console.log("refreshDatatable");
	$('#conectadosDataTable').DataTable().ajax.reload();//dataTable().fnReloadAjax();;
}
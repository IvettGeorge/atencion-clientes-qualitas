function getChatStr()
{

	viewCSitacomm();
	/*
	let  headers= {
    	"Content-Type": "aplication/json",
    	"access-control-allow-origin": "*",
	}
    axios.get(KEY_QUALITAS_ATC_CONFIG.table_chat_service+'/chat',headers).then(function (response) {
        console.log("resp chat");
            console.log(response.data[0]);
            var chat=response.data[0];
            console.log(chat.idChat);
            //chat.idChat=2;
            if(chat.idChat=='0')
            {
                viewCArena();
            }           
            else
            {
                viewCSitacomm();
            }
    }).catch(function (error) {
        console.log(error);
        muestraError("Chat No válido");  
    });*/
    
}


function viewCArena()
{
    console.log("Chat Arena");
    jQuery('#chatArena').css("display","block");
   
    jQuery('#chatSitacomm').css("display","none");

}

function viewCSitacomm()
{
    console.log("Chat Sitacomm");
    jQuery('#chatSitacomm').css("display","block");
    jQuery('#chatArena').css("display","none");
   
}

function initchat(){
					
	var confiig =KEY_QUALITAS_ATC_CONFIG;	
	var paraams=getParametersAccess();	
	doinitchat(paraams, confiig  );
			
}



var chatconnection;
var chatmessages= [];

var chatconfig_;
var chatparameters_;

function doinitchat(   parameters, configchat) {
	chatparameters_=parameters;
	chatconfig_= configchat;

	chatconnection = new ReconnectingWebSocket(chatconfig_.chat_service);

	chatconnection.onopen = (event) => {
		console.log(" chat is open now.");
		//commandLastMessages	();
    };

    chatconnection.onclose = (event) => {
      console.log(" is closed now.");
    };

    chatconnection.onerror = (event) => {
      console.error(" error observed:", event);
    };

    chatconnection.onmessage = (event) => {

		const data = JSON.parse(event.data);
		const msg ={
			timestamp	: Date.now(),
            nombre		: data.nombre,
			username	: data.username,
			message		: data.message,			
			
            email		: data.email,
			office		: data.office,
			job		    : data.job,
			region		: data.region,
            code		: data.code,
			
			messageResponse		: data.messageResponse,
			idMessageResponse 	: data.idMessageResponse,
			nombreResponse 		: data.nombreResponse,
			idMessage: data.idMessage
		}

		chatmessages.push(msg);
		renderChatMessages(msg);
    };
}


renderChatMessages = (message) => {
	$("#totalMessages").html("");
	if(message){
		$("#chat").append(getHtmlChatMessage(message, chatmessages.length+1 ));
		$("#totalMessages").append(messageChatTotal());
		$("#chat").animate({ scrollTop: $('#chat').prop('scrollHeight')}, 1000);
		return;
	}
	$("#chat").html("");
	for(var i = 0; i < chatmessages.length; i++ ){
		let message= chatmessages[i];
		$("#chat").append(getHtmlChatMessage(message, i));
	}
	$("#totalMessages").append(messageChatTotal());
	$("#chat").animate({ scrollTop: $('#chat').prop('scrollHeight')}, 1000);
}

messageChatTotal = () => {
	return '<p><strong>'+chatmessages.length+' mensajes</strong></p>';
}


function getHtmlChatMessage(message, indice){
	let formattedName=message.nombre;//
	let formattedMessage =message.message;
	let messageResponse =message.messageResponse;
	let idMessageResponse= message.idMessageResponse;
	let nombreResponse= message.nombreResponse;
	//this.parseUrls(message.message);
	
	
	let msghtml="";
	if(message.picture=="none"){
		message.picture==chatconfig_.default_avatar;//"s3.us-east-2.amazonaws.com/techdaycva.com/img/user.jpg";
	}
	
	if(indice % 2!=0){


		msghtml= '<ul class="list-unstyled media-block">'+
		'<li class="mar-btm ">'+    							
    		'<div class="media-body pad-hor">'+
				'<div class="row">'+
					'<div class="col-1 colImageChat">'+
					 '<img src="https://atencionaclientesqualitas.mx/static/lib/images/user.png" style="width:30px;">'+
					'</div>'+
					'<div class="col-8 colMessageChat messageLeft">'+
						'<div class="speech">'+
							'<div class="row">'+

							(
								( hasValue(messageResponse) && hasValue(idMessageResponse) && hasValue(nombreResponse) )? 
									(
										'<div class="col-12">'+
											'<p class="textName nameLeft"><strong>'+message.nombre+ ' </strong></p>'+
										'</div>'+
										
										'<div class="col-12 divMessage">'+

											'<div class="col-1"></div><div class="col-10" style="padding-left:0px;">'+
												'<p class="textNameResponse nameLeft"><strong> '+nombreResponse+'</strong></p>'+
											'</div><div class="col-1"></div>'+
											'<div class="col-1"></div><div class="col-10 divMessageResponse">'+
												'<p class="textMessageResponse"><strong>'+messageResponse+ '</strong></p>'+
											'</div><div class="col-1"></div>'+
											'<div class="textMessage col-12"><strong>'+formattedMessage+ '</strong></div>'+
										'</div>'
									)

								: (
									'<div class="col-12">'+
										'<p class="textName nameLeft"><strong> '+message.nombre+'</strong></p>'+
									'</div>'+
									'<div class="col-12 divMessage">'+
									'<p class="textMessage"><strong>'+formattedMessage+ '</strong></p>'+
									'</div>'
								)
							)+								
							'</div>'+
						'</div>'+
					'</div>'+					
					'<div class="col-1 colReply">'+
						"<a href='javascript:respondMessage("+message.idMessage+",\""+formattedMessage+"\",\""+ message.nombre+"\");' class='aReply'><i class='fa-solid fa-reply' aria-hidden='true'></i></a>"+
						/*'<button type="button" class="btn" data-toggle="dropdown">'+
							'<i class="fa-solid fa-reply" aria-hidden="true"></i>'+
						'</button>'+
						'<ul class="dropdown-menu dropdown-menu-right">'+
							"<li><a class='dropdown-item ' href='javascript:respondMessage("+message.idMessage+",\""+formattedMessage+"\",\""+ message.nombre+"\");'>Responder</a></li> "+          
							'<li class="divider"></li>'+             
						'</ul>'+*/
					'</div>'+					
				'</div>'+
    		'</div>'+
    	'</li></ul>';

	}else {
			msghtml= '<ul class="list-unstyled media-block">'+
			'<li class=" mar-btm ">'+    							
    			'<div class="media-body pad-hor speech-right">'+
					'<div class="row">'+
						'<div class="col-1">'+
						'</div>'+							
						'<div class="col-1 colReply">'+
							"<a href='javascript:respondMessage("+message.idMessage+",\""+formattedMessage+"\",\""+ message.nombre+"\");' class='aReply'><i class='fa-solid fa-reply' aria-hidden='true'></i></a>"+
							/*'<button type="button" class="btn" data-toggle="dropdown">'+
								'<i class="fa-solid fa-reply" aria-hidden="true"></i>'+
							'</button>'+
							'<ul class="dropdown-menu dropdown-menu-right">'+
								"<li><a class='dropdown-item ' href='javascript:respondMessage("+message.idMessage+",\""+formattedMessage+"\",\""+ message.nombre+"\");'>Responder</a></li> "+          
								'<li class="divider"></li>'+             
							'</ul>'+*/
						'</div>'+							
						'<div class="col-8 colMessageChat messageRight">'+		
    						'<div class="speech" style="padding-bottom:5px;">'+
								'<div class="row">'+

								(
									( hasValue(messageResponse) && hasValue(idMessageResponse) && hasValue(nombreResponse) )? 
										(
											'<div class="col-12">'+
		    								'<p class="textName nameRigth"><strong>'+formattedName+'</strong></p>'+
											'</div>'+
											
											'<div class="col-12 divMessage">'+

												'<div class="col-1"></div><div class="col-12" style="padding-right:0px;">'+
												'<p class="textNameResponse nameRigth"><strong> '+nombreResponse+'</strong></p>'+
												'</div><div class="col-1"></div>'+
												'<div class="col-1"></div><div class="col-0 divMessageResponse">'+
													'<div class="redimensionable textMessageResponse">'+messageResponse+ '</div>'+
												'</div><div class="col-1"></div>'+
		    									'<div class="col-12 redimensionable textMessage">'+formattedMessage+ '</div>'+
											'</div>'
										)
	
									: 	(

										'<div class="col-12">'+
		    								'<p class="textName nameRigth"><strong>'+formattedName+'</strong></p>'+
										'</div>'+
										'<div class="col-12 divMessage">'+
		    								'<div class="redimensionable textMessage">'+formattedMessage+ '</div>'+
										'</div>'
										)
	
								)+
										
								'</div>'+
    						'</div>'+
						'</div>'+
						'<div class="col-1 colImageChat">'+
							'<img src="https://atencionaclientesqualitas.mx/static/lib/images/user.png" style="width:30px;">'+
					   '</div>'+   
					'</div>'+
    			'</div>'+
    		'</li></ul>';
	}

	return msghtml;
}

function hasValue(value){
	return value !=undefined && value!=null && value!="";
}

function handleOnClick (e)  {
	sendMessageNow();
 }

function handleText(e){
	if (e.which === 13 && !e.shiftKey) {
    e.preventDefault();
    sendMessageNow();
    return false;
  }
}

function sendMessageNow(){
	message = $("#message").val();
	let messageResponse=_messageResponse !=null ? _messageResponse:"";//$("#messageResponse").val();
	let idMessageResponse=_idMessageResponse!=null ? _idMessageResponse: "";//$("#idMessageResponse").val();
	let nombreResponse=_nombreResponse !=null ? _nombreResponse:"";

	let closeDivResponse=hasValue(_idMessageResponse) && hasValue(_messageResponse) && hasValue(_nombreResponse);
	

	if (message && message !="") {

		var msg={
					//timestamp	: item.createdat,
					message		: 	message,			
					email		: chatparameters_.email,
                    nombre		: chatparameters_.nombre,
                    username	: chatparameters_.username,                  	                   
                    email		: chatparameters_.email,
                    office		: chatparameters_.office,
                    job		    : chatparameters_.job,
                    region	    : chatparameters_.region,
                    code	    : chatparameters_.code,

					messageResponse: messageResponse,
					idMessageResponse : idMessageResponse,
					nombreResponse: nombreResponse,
					command		: 'send'
		}

        const data = {
          "action": "sendmessage",
		  "data": JSON.stringify(msg)

        };

		chatconnection.send(JSON.stringify(data));

		if(closeDivResponse){
			try{
				$("#alert_messageResponse").alert('close');
			} catch (error) {
				console.error(error);
			}
		}
    }
	message = $("#message").val("");
	$("#message").focus();
}

function lastChatMessages(){
	$("#chat").html("");
	chatmessages= [];
	var  headers= {
          "Content-Type": "aplication/json",
          "access-control-allow-origin": "*",
      }
	axios.post(chatconfig_.table_chat_service+"/messages", {
		"timekey": "",
		"lastidkey": "",
		"pagesize": "",
        "command": "getAllMessages",
	}, headers).then(function (response) {
			//console.log(response.data);
			chatmessages= [];
			//var items = JSON.parse(response.data.body);
			var items = response.data;//.body;
			//console.log(items);
			for (var i =0; i<items.length ;i++) {
				let item = items[i];

				chatmessages.push({
					//timestamp	: item.createdat,
                    message		: item.message,			
					email		: item.email,
                    nombre		: item.nombre,
                    username	: item.username,                  	                   
                    email		: item.email,
                    office		: item.office,
                    job		    : item.job,
                    region	    : item.region,
                    code	    : item.code,
					idMessage	: item.idMessage,
					messageResponse: item.messageResponse,
					idMessageResponse : item.idMessageResponse,
					nombreResponse : item.nombreResponse
				});
			}
			renderChatMessages();

    }).catch(function (error) {
		console.log(error);
    });
}

var _messageResponse=null;
var _idMessageResponse=null;
var _nombreResponse=null;

function respondMessage(idMessageResponse,messageResponse,nombreResponse){

	_messageResponse=messageResponse;
	_idMessageResponse=idMessageResponse;
	_nombreResponse=nombreResponse;

	console.log("idMessageResponse==",idMessageResponse);
	console.log("messageResponse==",messageResponse);

	$('#div_messageResponse').html('');

	let html_alert=
	'<div class="alert alert-succes alert-dismissible fade show divMessageToRespond"  id="alert_messageResponse" role="alert">'+
		'<span id="nombreResponse" style="font-size: 12px; color:#fff; font-weight:bold;" > Respondiendo a '+nombreResponse+'<br/></span>'+
        '<span id="messageResponse" style="font-size: 12px; color:#fff;" > '+messageResponse+'</span>'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
        '<span aria-hidden="true">&times;</span>'+
        '</button>'+
    '</div>';

	
	$('#div_messageResponse').html(html_alert);

	$("#alert_messageResponse").on('closed.bs.alert', function () {
		// do something…
		_messageResponse=null;
		_idMessageResponse=null;
		_nombreResponse=null;

	  })

}
			
function validateOk()
{
    alertValidate("success","fa fa-check-circle","Validado Correctamente");   
}

function errorValidate(message)
{
    alertValidate("danger","fa fa-exclamation-triangle",message);   

}

function alertValidate(tipe,icon,msg) {
    $('#alert_placeholderValidate').html('<div class="alert alert-'+tipe+'"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-exclamation-triangle"></i><strong>  ' + msg + '&nbsp;&nbsp; </strong></div>');
 }


 function errorValidateSurvey(message)
{
    alertValidateSurvey("danger","fa fa-exclamation-triangle",message);   

}

function alertValidateSurvey(tipe,icon,msg) {
    $('#alert_placeholderSurvey').html('<div class="alert alert-'+tipe+'"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-exclamation-triangle"></i><strong>  ' + msg + '&nbsp;&nbsp; </strong></div>');
 }

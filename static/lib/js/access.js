
var KEY_QUALITAS_ATC_CONFIG={

	table_chat_service:"https://kfy74gw3k9.execute-api.us-east-2.amazonaws.com/Prod",//
	chat_service:"wss://3youekn263.execute-api.us-east-2.amazonaws.com/Prod" ,
	quizz_service:"" ,
	table_quizz_service:"https://kfy74gw3k9.execute-api.us-east-2.amazonaws.com/Prod",
	access_service:"wss://3vo5718k8l.execute-api.us-east-2.amazonaws.com/Prod",
	table_access_service:"https://kfy74gw3k9.execute-api.us-east-2.amazonaws.com/Prod",
	questions_service:"" ,
	table_questions_service:"" ,
};



function initaccess(){					
    var confiig =KEY_QUALITAS_ATC_CONFIG;	
    var paraams=getParametersAccess();	

    doAccess(paraams, confiig  );
        
}

function getParametersAccess(){
	let parameters={};
	parameters.nombre=$("#userSurvery").val();;
	parameters.username=$("#userSurvery").val();;
	parameters.email=$("#emailSurvery").val();
	parameters.office=$("#officeSurvery").val();
	parameters.job='';
	parameters.region=$("#regionSurvery").val();
	parameters.code='';	
	return parameters;
}

//----------------------


var connectionAccess;
var users_= [];
var connections_= [];
var usersByEmail_= [];

var configchatAccess_;
var parametersAccess_;
var renderUsersCallback_;

function doAccess(  parameters, configchat) {
	parametersAccess_=parameters;
	configchatAccess_= configchat;
	renderUsersCallback_= renderUsersJustTotal;// renderUsers esta linea indica la funcion para renderizar

	connectionAccess = new ReconnectingWebSocket(configchatAccess_.access_service);	
			
	connectionAccess.onopen = (event) => {	
		console.log(" access is open now.");					
		commandSendAccess();		
    };

    connectionAccess.onclose = (event) => {
      console.log(" is closed now.");
	  
    };

    connectionAccess.onerror = (event) => {
      console.error(" error observed:", event);
    };

    connectionAccess.onmessage = (event) => {
	 			
		const data = JSON.parse(event.data);    
        
		const user ={			
			timestamp	: Date.now(),           
			username	: data.username,			
			nombre		: data.nombre,
            email		: data.email,	
			office		: data.office,
			job			: data.job,					
			region		: data.region,				
			idDetail	: data.idDetail,
			activo		: data.activo
		}		
		renderUsersCallback_(user);
    };
}

commandSendAccess=()=>{
		
	var msg={
		username	: 	parametersAccess_.username, 					
		nombre		: 	parametersAccess_.nombre,
		email		:	parametersAccess_.email,
        office		:   parametersAccess_.office,
		job			:   parametersAccess_.job,					
		region		:   parametersAccess_.region,
		command		: 'sendAccess'
	}
								
    const data = {
        "action": "sendmessage",
		"data": JSON.stringify(msg)
    };
	let datasend=JSON.stringify(data)
    connectionAccess.send(datasend);         
}


renderUsersJustTotal = (user) => {

	if(user){		
		//let display=displayuser(user);
		//if(display){
			//$("#contacts").prepend(getContactoHtmlMessage(user ));		
		//}

		connections_.push({idDetail:user.idDetail});		
		users_.push(user);

		$("#totalContactos").html("");
		$("#totalContactos").append(messageTotalUser());			
		
		return;
	}

	//users_=obtainUsersConnected();
	//users_=filtereduserbymail();
	users_=usersByEmail_;

	$("#contacts").html("");
	
	$("#totalContactos").html("");
	$("#totalContactos").append(messageTotalUser());	
}

messageTotalUser = () => {
	return '<p><strong><i class="fa fa-user"></i>'+users_.length+'</strong></p>';
}


function displayuser(user){
	var connmap = getMapConnections();	
	var useremailmap=getmapuserbyemail();

	let existeCon=user.activo && connmap.get(user.idDetail);
	let existeEmail=user.activo && useremailmap.get(user.email);
	let esSameUsuario=(user.email == parametersAccess_.email);//username 
	if(existeCon ){
		return false;
	}
	if(existeEmail ){
		return false;
	}
	if(esSameUsuario){
		return false;
	}
	return true;
}

function obtainUsersConnected(){
	var usermap = getmapuserbyconnections();
	var connmap = getMapConnections();
	var usersfilteredDetail=[];
	for (var [clave, user] of usermap) {
		
		let existe=user.activo && connmap.get(user.idDetail);
		if(existe){
			usersfilteredDetail.push(user);
		}		
	}
	return usersfilteredDetail;
}


function filtereduserbymail(){
	var usermap =  new Map();
	var usersfiltered=[];
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){	
			usermap.set(user.email,user);			
			usersfiltered.push(user);
		}
	}
	return usersfiltered;
}


function getmapuserbyconnections(){
	var usermap = new Map();
	
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.idDetail); 
		if(!existe){
			usermap.set(user.idDetail,user);			
		}
	}
	return usermap;
}

function getmapuserbyemail(){
	var usermap = new Map();
	
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){
			usermap.set(user.email,user);			
		}
	}
	return usermap;
}


function getMapConnections(){
	var connmap = new Map();
	
	for(var i = 0; i < connections_.length; i++ ){
		let con= connections_[i];
		
		let existe=connmap.get(con.idDetail); 
		if(!existe){
			connmap.set(con.idDetail,con);			
		}
	}
	return connmap;
}

function lastUsers(){
	
	var usersTmp= [];
	var connectionsTmp=[];
	var usersByEmailTmp= [];
						
	var  headers= {
          "Content-Type": "aplication/json",
          "access-control-allow-origin": "*",		 
      }
	axios.post(configchatAccess_.table_access_service+"/details", { 	               
	}).then(function (response) {
			//console.log(response.data);						
			var items = JSON.parse(response.data.items);//body.items//
			for (var i =0; i<items.length ;i++) {
				let item = items[i];								
				usersTmp.push({
					timestamp	: item.createdat,
					username	: item.username,										
					nombre		: item.nombre,
					office      :   item.office,
	                job         :   item.job,
	                region      :   item.region,
	                code        :   item.code,	
					email		:   item.email,
					idDetail	:	item.idDetail,
					activo		:   item.activo					
				});				
			}

			var itemsByEmail = JSON.parse(response.data.users_filtered_by_email);//body.items//
			for (var i =0; i<itemsByEmail.length ;i++) {
				let item = itemsByEmail[i];								
				usersByEmailTmp.push({
					timestamp	: item.createdat,
					username	: item.username,										
					nombre		: item.nombre,
					office      :   item.office,
	                job         :   item.job,
	                region      :   item.region,
	                code        :   item.code,	
					email		:   item.email,
					idDetail	:	item.idDetail,
					activo		:   item.activo					
				});				
			}

			var conns = JSON.parse(response.data.connections);//body.connections
			for (var i =0; i<conns.length ;i++) {
				let con = conns[i];
				connectionsTmp.push({idDetail:con.connectionId});
			}
			
			users_ = usersTmp;
			usersByEmail_=usersByEmailTmp;
			connections_ = connectionsTmp;

			renderUsersCallback_();		
    }).catch(function (error) {
		console.log(error);
    });
}

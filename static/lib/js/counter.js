function getTime() {
    now = new Date();

    fullDate = "2022 05 06 08:30:00";
    fecha = new Date(fullDate);

    if(Number.isNaN(fecha.getMonth()))
    {
       arr = fullDate.split(/[- :]/);
     fecha = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
    }

    days = (fecha - now) / 1000 / 60 / 60 / 24;
    daysRound = Math.floor(days);
    hours = (fecha - now) / 1000 / 60 / 60 - (24 * daysRound);
    hoursRound = Math.floor(hours);
    minutes = (fecha - now) / 1000 / 60 - (24 * 60 * daysRound) - (60 * hoursRound);
    minutesRound = Math.floor(minutes);
    seconds = (fecha - now) / 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
    secondsRound = Math.round(seconds);
    if (daysRound <= "-1") {
    }
    else {
        document.getElementById('dias').innerHTML = daysRound;
        document.getElementById('horas').innerHTML = hoursRound;
        document.getElementById('min').innerHTML = minutesRound;
        document.getElementById('seg').innerHTML = secondsRound;
    }
    newtime = window.setTimeout("getTime();", 1000);
}
/* We'll be drawing some grids! */
var N_GRIDS = 2 /* increase for more grids */, 
		BASE_CELL_EDGE = 10 /* smallest gridcell */, 
		UNIT = 25 /* relative unit displacement */, 
		f = UNIT/N_GRIDS, 
		grids = [] /* array to keep grids in */, 
		opts = ['fill', 'stroke'], 
		no = opts.length, 
		c = document.querySelector('canvas'), 
		ct = c.getContext('2d'), 
		w = c.width, h = c.height;

var Cell = function(x, y) {
	this.x = x;
	this.y = y;
	this.f = []; // frequencies
	this.φ = []; // initial phases
	this.α = []; // opacities
	
	this.init = function() {
		for(var i = 0; i < no; i++) {
			// make them small don't want crazy fast
			this.f.push((.1 + Math.random())/10000);
			this.φ.push(2*Math.PI*Math.random());
			this.α.push(
				Math.min(
					Math.max(0, Math.sin(this.φ[i]) - .5), 
					Math.max(0, 1 - this.y/h/Math.pow(this.x/w, 2))
				)
			);
		}
	};
	
	this.draw = function(t, d) {
		for(var i = 0; i < no; i++) {
			if(this.y/h < Math.pow(this.x/w, 2)) {
				if(this.α[i] > 0) {
					this.α[i] -= .01;
					this.α[i] = Math.max(this.α[i], 0);
				}
			}
			else {
				this.α[i] = Math.max(
					0, 
					Math.sin(this.f[i]*t + this.φ[i]) - .5
				);
			}
			
			ct.globalAlpha = this.α[i];
			ct[opts[i] + 'Rect'](this.x, this.y, d, d);
		}
	}
	
	this.init();
};

var Grid = function(d) {
	this.d = d; // cell edge for this grid
	this.rows = ~~(h/this.d) + 1;
	this.cols = ~~(w/this.d) + 1;
	this.w = this.cols*this.d;
	this.h = this.rows*this.d;
	this.cells = [];
	this.n = 0;
	this.v = this.d/UNIT; // grid motion velocity
	
	this.init = function() {
		for(var row = 0; row < this.rows; row++) {
			for(var col = 0; col < this.cols; col++) {
				this.cells.push(new Cell(
					(col - 1)*this.d, 
					(row - 1)*this.d + (h%this.d)
				));
			}
		}
		
		this.n = this.cells.length;
	};
	
	this.update = function(t) {
		for(var i = 0; i < this.n; i++) {
			this.cells[i].x += this.v;
			this.cells[i].y -= this.v;
			
			if(this.cells[i].x > this.w - this.d) {
				this.cells[i].x -= this.w;
			}
			if(this.cells[i].y < h - this.h) {
				this.cells[i].y += this.h;
			}
			
			this.cells[i].draw(t, this.d);
		}
	};
	
	this.init();
};

(function init() {	
	for(var i = 0; i < N_GRIDS; i++) {
		grids.push(new Grid((i + 1)*BASE_CELL_EDGE));
	}
})();

(function ani(t) {
	var g = ct.createLinearGradient(0, h, w, 0), u;
	
	ct.clearRect(0, 0, w, h);
	
	for(var i = 0; i < N_GRIDS; i++) {
		u = i*f;
		g.addColorStop(
			0, 'black'
		);
		g.addColorStop(
			1, 'grey'
		);
		ct.fillStyle = ct.strokeStyle = g;

		grids[i].update(t);
	}
	
	requestAnimationFrame(ani.bind(this, ++t));
})(0);
'use-strict'
const ASW = require('aws-sdk');
const documentClient = new ASW.DynamoDB.DocumentClient();

//qualitas-brindis-access-st-SendMessageFunctionRole-RVZNZ49KEIKN 
exports.handler = async (event, context) => {
    
    let responseBody = "";
    let statusCode = 0;

try {
        


    const params = {
        TableName: "qualitas_atc_access_details",
        FilterExpression: " #activo = :vActivo",
        ExpressionAttributeNames: {           
            "#activo": "activo",
        },
        ExpressionAttributeValues: {           
            ":vActivo": true
        }
    };
    
    const paramsConnections = {
        TableName: "qualitas_atc_access_connections",
    };

    var allDataConn=new Array;
    await getAllData(paramsConnections, allDataConn);
    
    
    var allData=new Array;
    await getAllData(params, allData);

    console.log("allData.lenght"+allData.length);
    console.log("allDataConn.lenght"+allDataConn.length);

    //console.log("allData==",allData);
    //console.log("allDataConn==",allDataConn);
    
    var users_conectados=obtainUsersConnected(allData, allDataConn);
    
    console.log("users_conectados.lenght"+users_conectados.length);
      

    var users_filteredbyemail=filtereduserbymail(users_conectados);

    console.log("users_filteredbyemail.lenght"+users_filteredbyemail.length);

    var allDataFinal=users_filteredbyemail;
    ///var allDataFinalx=filterconectadosbyemail(allData);
    console.log("finallenght"+allDataFinal.length);
    let messages =allDataFinal.sort((a, b) => a.createdat - b.createdat);
              
    responseBody = JSON.stringify(messages);
       
    statusCode = 200;
        
    } catch(error) {
        console.log(error);
       
        responseBody = error;
        statusCode = 403;
    }
    

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "aplication/json",
            "Access-Control-Allow-Origin": "*"
        },
        body:responseBody
    };

    return response;
};

function filterconectadosbyemail(usersconectados){
	var usermap = new Map();
	var allDatayByEmail=new Array;
	for(var i = 0; i < usersconectados.length; i++ ){
		let user= usersconectados[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){
			usermap.set(user.email,user);			
		}
	}
	usermap.forEach(function(item) {
          allDatayByEmail.push(item); 
    });
	return allDatayByEmail;
}


function filtereduserbymail(users_){
	var usermap =  new Map();
	var usersfiltered=[];
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){	
			usermap.set(user.email,user);			
			usersfiltered.push(user);
		}
	}
	return usersfiltered;
}


function obtainUsersConnected(users_, connections_){
    //console.log("*** obtainUsersConnected");
    	
	var usermap = getmapuserbyconnections(users_);
	var connmap = getMapConnections(connections_);
	var usersfilteredDetail=[];
	for (var [clave, user] of usermap) {
		
		//console.log("clave, user",clave, user);
    
		let existe=user.activo && connmap.get(user.idDetail);
		//console.log("connmap.get(user.idDetail)",(connmap.get(user.idDetail)));
		//console.log("existe=",existe);
		if(existe){
			usersfilteredDetail.push(user);
		}		
	}
	return usersfilteredDetail;
}

function getmapuserbyconnections(users_){
     //console.log("*** getmapuserbyconnections");
	var usermap = new Map();
	
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.idDetail); 
		if(!existe){
			usermap.set(user.idDetail,user);			
		}
	}
	//console.log("**resutl usermap==", usermap);
	return usermap;
}

function getMapConnections(connections_){
    //console.log("*** getMapConnections");
	var connmap = new Map();
	
	for(var i = 0; i < connections_.length; i++ ){
		let con= connections_[i];
		
		let existe=connmap.get(con.connectionId); 
		if(!existe){
			connmap.set(con.connectionId,con);			
		}
	}
	//console.log("**resutl connmap==", connmap);
	return connmap;
}



const getAllData = async (params, allData) => { 

    //console.log("Querying Table");
    const data = await documentClient.scan(params).promise();

    if(data.Items){
        
       data.Items.forEach(function(item) {
          allData.push(item); 
       });
    }

   
    if (data.LastEvaluatedKey ) {
        params.ExclusiveStartKeyata.LastEvaluatedKey;
        return await getAllData(params, allData);

    } else {
        return data;
    }
}

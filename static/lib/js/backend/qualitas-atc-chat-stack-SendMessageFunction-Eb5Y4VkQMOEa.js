 // Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

const AWS = require('aws-sdk');


const ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10', region: process.env.AWS_REGION });


const { TABLE_NAME } = process.env;
const  TABLE_MESSAGES='qualitas_atc_chat_messages';


exports.sendMessage = async event => {
  
 //let res=await createMessageMock();
 //console.log("res===", res);
 //return;
	let postData = JSON.parse(event.body).data;
	let alldata =JSON.parse(postData);
	
    let control=alldata.command;
    console.log(control);

//SE CREA MENSAJE CON TODOS LOS DATOS
	const connectionId= event.requestContext.connectionId;
	
	try {
		let idMessage=await createMessage(connectionId, alldata); 
		alldata['idMessage']=idMessage;
		postData=JSON.stringify(alldata);
	} catch (e) {
		//console.log(e);
		return { statusCode: 500, body: e };
	}

//SE ENVIA MENSAJE A TODOS LOS SUSCRITOS
  let connectionData;
 
  try {
    connectionData = await ddb.scan({ TableName: TABLE_NAME, ProjectionExpression: 'connectionId' }).promise();
  } catch (e) {
    return { statusCode: 500, body: e.stack };
  }
 
  const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
  });
 
 
  const postCalls = connectionData.Items.map(async ({ connectionId }) => {
    try {
      await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: postData  }).promise();
    } catch (e) {
      if (e.statusCode === 410) {
        console.log(`Found stale connection, deleting ${connectionId}`);
        await ddb.delete({ TableName: TABLE_NAME, Key: { connectionId } }).promise();
      } else {
        throw e;
      }
    }
  });
 
  try {
    await Promise.all(postCalls);
  } catch (e) {
    return { statusCode: 500, body: e.stack };
  }


  return { statusCode: 200, body: 'Data sent.' };
};



async function createMessage(connectionId, alldata){
 
  let id=""+Date.now();
  const putParams = {
    TableName: TABLE_MESSAGES,
    Item: {
      idMessage 	: id,//
      connectionId 	:connectionId,
      username 		: alldata.username,
      message		: alldata.message,
      nombre 		:alldata.nombre,      
      email	        :alldata.email,
      office	    :alldata.office,
      job	        :alldata.job,     
      region	    :alldata.region,      
      createdat: id,
      messageResponse:alldata.messageResponse,
      idMessageResponse:alldata.idMessageResponse,
      nombreResponse: alldata.nombreResponse
      
    }
  };

  try {
    await ddb.put(putParams).promise();
    return id;
  } catch (err) {    
    return { statusCode: 500, body: 'Failed at create mmessages to connect: ' + JSON.stringify(err) };
  }
}

async function createMessageMock(){
 console.log("mock");
 let x="1"+Date.now();
  const putParams = {
    TableName: TABLE_MESSAGES,
    Item: {
      idMessage 	: x, //"1"+Date.now(),
      connectionId 	:"1",
      username 		: "1",
      message		:"1",
      nombre 		:"1",
      numempleado	:"1",
      picture		:"1",
      createdat: x//"1"+Date.now()
    }
  };
 console.log("try mock");
  try {
    console.log("to executed mock");
     await ddb.put(putParams).promise();
     console.log("executed mock");
     return x;
  } catch (err) {   
    console.log(err);
    return { statusCode: 500, body: 'Failed at create mmessages to connect: ' + JSON.stringify(err) };
  }
}
'use-strict'
const ASW = require('aws-sdk');
const documentClient = new ASW.DynamoDB.DocumentClient();


exports.handler = async (event, context) => {
    
    let responseBody = "";
    let statusCode = 0;

try {
        const params = {
            TableName: "qualitas_atc_access_details",
            FilterExpression: " #activo = :vActivo",
            ExpressionAttributeNames: {           
                "#activo": "activo",
            },
            ExpressionAttributeValues: {           
                ":vActivo": true
            }
        };
        
        const paramsConnections = {
            TableName: "qualitas_atc_access_connections",
        };
        var allDataConn=new Array;
        await getAllData(paramsConnections, allDataConn);
        
        
        var allData=new Array;
        await getAllData(params, allData);
        //console.log("Processing Completed");

        console.log("finallenght"+allData.length);
        let messages =allData.sort((a, b) => a.createdat - b.createdat);
       
        var users_conectados=obtainUsersConnected(allData, allDataConn);
        var users_filteredbyemail=filtereduserbymail(users_conectados);


        responseBody = //JSON.stringify(messages);
        {  'items':JSON.stringify(messages),
           'connections':JSON.stringify(allDataConn),
           'users_filtered_by_email':JSON.stringify(users_filteredbyemail)
        };
         statusCode = 200;
        //console.log(messages);
    } catch(error) {
        console.log(error);
       
        responseBody = error;
        statusCode = 403;
    }
    

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "aplication/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(responseBody)
    };

    return response;
};


function filtereduserbymail(users_){
	var usermap =  new Map();
	var usersfiltered=[];
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.email); 
		if(!existe){	
			usermap.set(user.email,user);			
			usersfiltered.push(user);
		}
	}
	return usersfiltered;
}


function obtainUsersConnected(users_, connections_){
    //console.log("*** obtainUsersConnected");
    	
	var usermap = getmapuserbyconnections(users_);
	var connmap = getMapConnections(connections_);
	var usersfilteredDetail=[];
	for (var [clave, user] of usermap) {
		
		//console.log("clave, user",clave, user);
    
		let existe=user.activo && connmap.get(user.idDetail);
		//console.log("connmap.get(user.idDetail)",(connmap.get(user.idDetail)));
		//console.log("existe=",existe);
		if(existe){
			usersfilteredDetail.push(user);
		}		
	}
	return usersfilteredDetail;
}

function getmapuserbyconnections(users_){
     //console.log("*** getmapuserbyconnections");
	var usermap = new Map();
	
	for(var i = 0; i < users_.length; i++ ){
		let user= users_[i];
		
		let existe=usermap.get(user.idDetail); 
		if(!existe){
			usermap.set(user.idDetail,user);			
		}
	}
	//console.log("**resutl usermap==", usermap);
	return usermap;
}

function getMapConnections(connections_){
    //console.log("*** getMapConnections");
	var connmap = new Map();
	
	for(var i = 0; i < connections_.length; i++ ){
		let con= connections_[i];
		
		let existe=connmap.get(con.connectionId); 
		if(!existe){
			connmap.set(con.connectionId,con);			
		}
	}
	//console.log("**resutl connmap==", connmap);
	return connmap;
}


const getAllData = async (params, allData) => { 

    console.log("Querying Table");
    const data = await documentClient.scan(params).promise();

    if(data.Items){
        
       data.Items.forEach(function(item) {
          allData.push(item); 
       });
    }

   
    if (data.LastEvaluatedKey ) {
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        return await getAllData(params, allData);

    } else {
        return data;
    }
}

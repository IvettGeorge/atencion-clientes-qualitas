from django.forms import *

from core.models import *

from datetime import datetime

from django.shortcuts import render

class QuizzForm(ModelForm):

    class Meta:
            model = Quizz
            fields = '__all__'
            widgets = {
                'quizz': TextInput(
                    attrs={
                        'placeholder': 'Quizz',
                    }
                ),
                'answer': TextInput(
                    attrs={
                        'placeholder': 'Answer',
                    }
                ),

                'user': TextInput(
                    attrs={
                        'placeholder': 'Usuario',
                    }
                ),
                'email': TextInput(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
                'office': TextInput(
                    attrs={
                        'placeholder': 'Office',
                    }
                ),  
                'region': TextInput(
                    attrs={
                        'placeholder': 'Region',
                    }
                )                               
            }

class QuestionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['question'].widget.attrs['autofocus'] = True

    class Meta:
            model = Question
            fields = '__all__'
            widgets = {
                'question': Textarea(
                    attrs={
                        'placeholder': 'Pregunta al ponente',
                        #'rows': 5,
                        #'cols': 40,
                    }
                ),
                'user': TextInput(
                    attrs={
                        'placeholder': 'Usuario',
                    }
                ),
                'email': TextInput(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
                'office': TextInput(
                    attrs={
                        'placeholder': 'Office',
                    }
                ),  
                'region': TextInput(
                    attrs={
                        'placeholder': 'Region',
                    }
                )                               
            }

class SurveyForm(ModelForm):

    class Meta:
            model = Survey
            fields = '__all__'
            widgets = {
                'user': TextInput(
                    attrs={
                        'placeholder': 'User',
                    }
                ),
                'email': EmailInput(
                    attrs={
                        'placeholder': 'Email',
                    }
                ),
                'office': TextInput(
                    attrs={
                        'placeholder': 'Office',
                    }
                ),  
                'region': TextInput(
                    attrs={
                        'placeholder': 'Region',
                    }
                ),                               
                's1': TextInput(
                    attrs={
                        'placeholder': 'Survey 1',
                    }
                ),
                's1Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 1 Comment',
                    }
                ),
                 's2': TextInput(
                    attrs={
                        'placeholder': 'Survey 2',
                    }
                ),
                's2Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 2 Comment',
                    }
                ),
                's3': TextInput(
                    attrs={
                        'placeholder': 'Survey 3',
                    }
                ),
                's3Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 3 Comment',
                    }
                ),
                's4': TextInput(
                    attrs={
                        'placeholder': 'Survey 4',
                    }
                ),
                 's4Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 4 Comment',
                    }
                ),
                's5': TextInput(
                    attrs={
                        'placeholder': 'Survey 5',
                    }
                ),
                's5Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 5 Comment',
                    }
                ),
                's6': TextInput(
                    attrs={
                        'placeholder': 'Survey 6',
                    }
                ), 
                's6Comment': TextInput(
                    attrs={
                        'placeholder': 'Survey 6 Comment',
                    }
                ),
            }

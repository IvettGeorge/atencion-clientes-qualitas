from django.db import models
# Create your models here.
from datetime import datetime
# libs path_rename
from django.utils import timezone
import os
from uuid import uuid4
from django.utils.timezone import now
from user.models import User


class Quizz(models.Model):
    quizz=models.IntegerField(verbose_name='Quizz')
    answer=models.CharField(max_length=150,verbose_name='Answer')
    user=models.CharField(max_length=150,verbose_name='Name', blank=True)
    email=models.CharField(max_length=150,verbose_name='Mail', blank=True)
    office=models.CharField(max_length=150,verbose_name='Office', blank=True)
    region=models.CharField(max_length=150,verbose_name='Region', blank=True)

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    

    class Meta:
        db_table='quizz'
        verbose_name='Quizz'
        verbose_name_plural='Quizz'
        ordering=['id']
        
class Survey(models.Model):
    user=models.CharField(max_length=150,verbose_name='User',null=True)
    email=models.CharField(max_length=150,verbose_name='Email',null=True, blank=True)
    office=models.CharField(max_length=150,verbose_name='Office',null=True, blank=True)
    region=models.CharField(max_length=150,verbose_name='Region',null=True, blank=True)
    s1=models.CharField(max_length=150,verbose_name='Answer 1', null=True)
    s1Comment=models.CharField(max_length=250,verbose_name='Answer 1 Comment', null=True)

    s2=models.CharField(max_length=150,verbose_name='Answer 2', null=True)
    s2Comment=models.CharField(max_length=250,verbose_name='Answer 2 Comment', null=True)

    #s2A=models.CharField(max_length=150,verbose_name='Answer 2A',null=True)
    #s2B=models.CharField(max_length=150,verbose_name='Answer 2B',null=True)
    #s2C=models.CharField(max_length=150,verbose_name='Answer 2C',null=True)
    #s2D=models.CharField(max_length=150,verbose_name='Answer 2D',null=True)
    s3=models.CharField(max_length=150,verbose_name='Answer 3', null=True)
    s3Comment=models.CharField(max_length=250,verbose_name='Answer 3 Comment', null=True)
    s4=models.CharField(max_length=250,verbose_name='Answer 4',null=True)
    s4Comment=models.CharField(max_length=250,verbose_name='Answer 4 Comment', null=True)
    s5=models.CharField(max_length=250,verbose_name='Answer 5', null=True)
    s5Comment=models.CharField(max_length=250,verbose_name='Answer 5 Comment', null=True)
    s6=models.CharField(max_length=200,verbose_name='Answer 6',null=True)
    s6Comment=models.CharField(max_length=250,verbose_name='Answer 6 Comment', null=True)
   
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user

    class Meta:
        db_table='survey'
        verbose_name='Survey'
        verbose_name_plural='Surveys'
        ordering=['id']

class Question(models.Model):
    question=models.CharField(max_length=250, verbose_name='Question')
    user=models.CharField(max_length=100, verbose_name='user', blank=True)
    email=models.EmailField(verbose_name="Email")
    office=models.CharField(max_length=150,verbose_name='Office', blank=True)
    region=models.CharField(max_length=150,verbose_name='Region', blank=True)

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table='question'
        verbose_name='Question'
        verbose_name_plural='Question'
        ordering=['id']

class Active(models.Model):
    name=models.CharField(max_length=150, verbose_name='name')
    numberActive=models.IntegerField()

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table='Active'
        verbose_name='Active'
        verbose_name_plural='Actives'
        ordering=['id']
